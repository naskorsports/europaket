<?php

namespace naskorsports\europaket;

use Exception;
use naskorsports\europaket\Response\Token;
use Throwable;

class Request {

    /** @var Europaket */
    private $europaket;
    /** @var Token */
    private $token;

    private const API_URL_LOGIN = '/api/auth/login';

    private static $endpointUrlSandbox = "https://eps-api-eps-api-staging.azurewebsites.net";
    private static $endpointUrlProduction = "https://api.europaket.de";

    public static function createByEuropaket(Europaket $europaket): Request
    {
        $obj = new Request();
        $obj->europaket = $europaket;
        return $obj;
    }

    /**
     * @param string $string
     * @param string $url
     * @return string
     * @throws Exception
     */
    public function sendStringToUrl(string $string, string $url): string
    {
        try {
            $headers = ['Content-Type' => 'application/json'];
            if($url != self::API_URL_LOGIN) {
                $headers['Authorization'] = 'Bearer '.$this->getToken()->getAccessToken();
            }
            $client = new \GuzzleHttp\Client();
            $request = new \GuzzleHttp\Psr7\Request(
                'POST',
                self::$endpointUrlProduction.$url,
                $headers,
                $string
            );
            $this->europaket->getLogger() ? $this->europaket->getLogger()->debug(print_r((string)$request->getBody(),true)) : null;
            $response = $client->send($request);
            $this->europaket->getLogger() ? $this->europaket->getLogger()->debug(print_r((string)$response->getBody(),true)) : null;
        } catch (Throwable $e) {
            # todo: handle errors
            throw new Exception(null, null, $e);
        }
        return $response->getBody();
    }

    /**
     * @param string $url
     * @return string
     * @throws Exception
     */
    public function getFromUrl(string $url): string
    {
        try {
            $headers = ['Content-Type' => 'application/json'];
            if($url != self::API_URL_LOGIN) {
                $headers['Authorization'] = 'Bearer '.$this->getToken()->getAccessToken();
            }
            $client = new \GuzzleHttp\Client();
            $request = new \GuzzleHttp\Psr7\Request(
                'GET',
                self::$endpointUrlProduction.$url,
                $headers
            );
            $response = $client->send($request);
        } catch (Throwable $e) {
            # todo: handle errors
            throw new Exception(null, null, $e);
        }
        return $response->getBody();
    }

    /**
     * @return Token
     * @throws Exception
     */
    private function getToken(): Token
    {
        if(!isset($this->token) || !($this->token instanceof Token) || $this->token->isExpired()) {
            $responseString = $this->sendStringToUrl(
                json_encode([
                    'Username' => $this->europaket->getUsername(),
                    'Password' => $this->europaket->getPassword()
                ]),
                self::API_URL_LOGIN
            );
            $this->token = Token::createByJsonString($responseString);
        }
        return $this->token;
    }

}