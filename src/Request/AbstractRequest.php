<?php

namespace naskorsports\europaket\Request;

use naskorsports\europaket\Request;

abstract class AbstractRequest {

    /** @var Request */
    protected $request;

    /**
     * @param Request $request
     * @return static
     */
    public static function createByRequest(Request $request): AbstractRequest
    {
        $obj = new static();
        $obj->request = $request;
        return $obj;
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function sendPostWithRequest(): string
    {
        return $this->request->sendStringToUrl(json_encode($this->toArray()), $this->getUrl());
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function sendGet(): string
    {
        return $this->request->getFromUrl($this->getUrl());
    }

    abstract protected function toArray():array;
    abstract protected function getUrl():string;

}