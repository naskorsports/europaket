<?php

namespace naskorsports\europaket\Request;

use GuzzleHttp\Exception\GuzzleException;
use naskorsports\europaket\Response\Track as TrackResponse;

class TrackByDateRange extends AbstractRequest {

    /** @var \DatePeriod|null */
    private $dateRange;

    /**
     * @return int|null
     */
    public function getDateRange(): ?\DatePeriod
    {
        return $this->dateRange;
    }

    /**
     * @param \DatePeriod|null $dateRange
     * @return TrackByDateRange
     */
    public function setDateRange(?\DatePeriod $dateRange): TrackByDateRange
    {
        $this->dateRange = $dateRange;
        return $this;
    }

    /**
     * @return TrackResponse
     * @throws GuzzleException
     */
    public function send(): TrackResponse
    {
        return TrackResponse::createByJsonString($this->sendGet());
    }

    protected function toArray(): array
    {
        return [];
    }

    protected function getUrl(): string
    {
        $dateFrom = $this->getDateRange()->getStartDate()->format("Y-m-d%20H:i:s");
        $dateTill = $this->getDateRange()->getEndDate()->format("Y-m-d%20H:i:s");
        return "/api/track/parcel?dateFrom=$dateFrom&dateTill=$dateTill";
    }

}