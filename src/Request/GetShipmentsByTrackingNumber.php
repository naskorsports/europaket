<?php

namespace naskorsports\europaket\Request;

use naskorsports\europaket\Response\Shipment;

class GetShipmentsByTrackingNumber extends AbstractRequest
{

    /** @var string */
    private $trackingNumber;

    /**
     * @return string
     */
    public function getTrackingNumber(): string
    {
        return $this->trackingNumber;
    }

    /**
     * @param string $trackingNumber
     */
    public function setTrackingNumber(string $trackingNumber): void
    {
        $this->trackingNumber = $trackingNumber;
    }

    /**
     * @return \naskorsports\europaket\Response\Shipment[]
     * @throws \Exception
     */
    public function send(): array
    {
        $values = json_decode($this->sendGet(), true)['value'];
        $shipments = [];
        foreach ($values as $value) {
            $shipments[] = Shipment::createByJsonString(json_encode($value));
        }

        return $shipments;
    }

    protected function toArray(): array
    {
        return [];
    }

    protected function getUrl(): string
    {
        $trackingNumber = $this->getTrackingNumber();
        return "/api/shipments?search=TrackingNumber eq $trackingNumber";
    }


}
