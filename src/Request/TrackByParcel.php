<?php

namespace naskorsports\europaket\Request;

use GuzzleHttp\Exception\GuzzleException;
use naskorsports\europaket\Response\Track as TrackResponse;

class TrackByParcel extends AbstractRequest {

    /** @var string|null */
    private $statusHistoryHref;


    /**
     * @return TrackResponse
     * @throws \Exception
     */
    public function send(): TrackResponse
    {
        return TrackResponse::createByJsonString($this->sendGet());
    }

    protected function toArray(): array
    {
        return [];
    }

    /**
     * @return string|null
     */
    public function getStatusHistoryHref(): ?string
    {
        return $this->statusHistoryHref;
    }

    /**
     * @param string|null $statusHistoryHref
     */
    public function setStatusHistoryHref(?string $statusHistoryHref): void
    {
        $this->statusHistoryHref = $statusHistoryHref;
    }

    protected function getUrl(): string
    {
        return str_replace('https://api.europaket.de','',$this->getStatusHistoryHref());
    }
}
