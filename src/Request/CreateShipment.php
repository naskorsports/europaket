<?php

namespace naskorsports\europaket\Request;

use Exception;
use naskorsports\europaket\Helper\Address;
use naskorsports\europaket\Helper\Extra;
use naskorsports\europaket\Helper\Parcel;
use naskorsports\europaket\Request;
use naskorsports\europaket\Response\Shipment;

class CreateShipment extends AbstractRequest {

    public const DISPATCH_TYPE_ID_CHEAPEST = 2;
    public const DISPATCH_TYPE_ID_GLS_DE = 95;
//    public const DISPATCH_TYPE_ID_GLS_NL_BUSINESS = 101;
//    public const DISPATCH_TYPE_ID_GLS_NL = 102;
    public const DISPATCH_TYPE_ID_DHL_DE = 157;
    public const DISPATCH_TYPE_ID_DHL_NL = 214;
    public const DISPATCH_TYPE_ID_UPS_DE = 238;
    public const DISPATCH_TYPE_ID_UPS_DE_EXPRESS = 239;

//    public const SUBDIVISION_CODE_GENERAL = 0;

    // https://wikipedia.org/wiki/Incoterms
    public const INCOTERM_EXW = "EXW";
    public const INCOTERM_FCA = "FCA";
    public const INCOTERM_FAS = "FAS";
    public const INCOTERM_FOB = "FOB";
    public const INCOTERM_CFR = "CFR";
    public const INCOTERM_CIF = "CIF";
    public const INCOTERM_DAP = "DAP";
    public const INCOTERM_DPU = "DPU";
    public const INCOTERM_CPT = "CPT";
    public const INCOTERM_CIP = "CIP";
    public const INCOTERM_DDP = "DDP";

    /** @var string|null	MaxLength: 64	Mandatory */
    private $reference;
    /** @var int	Mandatory */
    private $dispatchTypeId = self::DISPATCH_TYPE_ID_CHEAPEST;
    /** @var int Can be used to work with different cost centers    Mandatory */
//    private $subdivisionCode = self::SUBDIVISION_CODE_GENERAL;
    private $subdivisionCode;
    /** @var string|null	MaxLength: 128	Optional*/
    private $description;
    /** @var string|null	MaxLength: 128	Optional */
    private $instructions;
    /** @var string|null	MaxLength: 3	Mandatory */
    private $incoterm;
    # todo: currency
    # todo: value
    /** @var Address|null */
    private $addressFrom;
    /** @var Address|null */
    private $addressTo;
    # todo: address invoice
    /** @var Address|null */
    private $addressInvoice;
    /** @var Extra|null */
    private $extra;
    /** @var Parcel[]|null */
    private $parcels = [];
    # todo: tracked
    # todo: customsDeclaration
    # todo: customsDeclarationItems

    protected function getUrl(): string
    {
        return "/api/shipments";
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function getDispatchTypeId(): int
    {
        return $this->dispatchTypeId;
    }

    public function getSubdivisionCode(): ?int
    {
        return $this->subdivisionCode;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getInstructions(): ?string
    {
        return $this->instructions;
    }

    public function getIncoterm(): ?string
    {
        return $this->incoterm;
    }

    public function getAddressFrom(): ?Address
    {
        return $this->addressFrom;
    }

    public function getAddressTo(): ?Address
    {
        return $this->addressTo;
    }

    public function getAddressInvoice(): ?Address
    {
        return $this->addressInvoice;
    }

    /**
     * @return Parcel[]|null
     */
    public function getParcels(): ?array
    {
        return $this->parcels;
    }

    public function send(): Shipment
    {
        return Shipment::createByJsonString($this->sendPostWithRequest());
    }

    /**
     * @param $referenceString
     * @return $this
     * @throws Exception
     */
    public function setReference($referenceString): CreateShipment
    {
        if(strlen($referenceString)>64) {
            throw new Exception("Invalid parameter");
        }
        $this->reference = $referenceString;
        return $this;
    }

    /**
     * @param int $dispatchTypeId
     * @return $this
     */
    public function setDispatchTypeId(int $dispatchTypeId): CreateShipment
    {
        $this->dispatchTypeId = $dispatchTypeId;
        return $this;
    }

    /**
     * @param int $subdivisionCode
     * @return $this
     */
    public function setSubdivisionCode(int $subdivisionCode): CreateShipment
    {
        $this->subdivisionCode = $subdivisionCode;
        return $this;
    }

    /**
     * @param string $description
     * @return $this
     * @throws Exception
     */
    public function setDescription(string $description): CreateShipment
    {
        if(strlen($description) > 128) {
            throw new Exception("Invalid parameter");
        }
        $this->description = $description;
        return $this;
    }

    /**
     * @param string $instructions
     * @return $this
     * @throws Exception
     */
    public function setInstructions(string $instructions): CreateShipment
    {
        if(strlen($instructions) > 128) {
            throw new Exception("Invalid parameter");
        }
        $this->instructions = $instructions;
        return $this;
    }

    public function setIncoterm(string $incoterm): CreateShipment
    {
        if(!in_array($incoterm,[
            self::INCOTERM_EXW,
            self::INCOTERM_FCA,
            self::INCOTERM_FAS,
            self::INCOTERM_FOB,
            self::INCOTERM_CFR,
            self::INCOTERM_CIF,
            self::INCOTERM_DAP,
            self::INCOTERM_DPU,
            self::INCOTERM_CPT,
            self::INCOTERM_CIP,
            self::INCOTERM_DDP
        ])) {
            throw new Exception("Invalid parameter");
        }
        $this->incoterm = $incoterm;
        return $this;
    }

    public function setAddressFrom(Address $address): CreateShipment
    {
        $this->addressFrom = $address;
        return $this;
    }

    public function setAddressTo(Address $address): CreateShipment
    {
        $this->addressTo = $address;
        return $this;
    }

    public function setAddressInvoice(Address $address): CreateShipment
    {
        $this->addressInvoice = $address;
        return $this;
    }

    /**
     * @param Parcel[] $parcels
     * @return CreateShipment
     * @throws Exception
     */
    public function setParcels(array $parcels): CreateShipment
    {
        foreach($parcels as $parcel) {
            if(!$parcel instanceof Parcel) {
                throw new Exception("Invalid parameter!");
            }
        }
        $this->parcels = $parcels;
        return $this;
    }

    /**
     * @return string[]
     */
    private function getMissingMandatoryKeys(): array
    {
        $mandatoryKeys = [
            'reference',
            'dispatchTypeId',
//            'subdivisionCode',
            'incoterm',
        ];
        $missingKeys = [];
        foreach($mandatoryKeys as $mandatoryKey) {
            if(!isset($this->$mandatoryKey)) {
                $missingKeys[] = $mandatoryKey;
            }
        }
        return $missingKeys;
    }

    public function getExtra(): ?Extra
    {
        return $this->extra;
    }

    /**
     * @param Extra|null $extra
     * @return CreateShipment
     */
    public function setExtra(?Extra $extra): CreateShipment
    {
        $this->extra = $extra;
        return $this;
    }

    /**
     * @throws Exception
     */
    public function toArray(): array
    {
        if(count($this->getMissingMandatoryKeys()) > 0) {
            throw new Exception(
                "Mandatory fields missing: "
                .implode(',',$this->getMissingMandatoryKeys())
            );
        }

        $shipmentArray = [
            'reference' => $this->getReference(),
            'dispatchTypeID' => $this->getDispatchTypeId(),
            'subdivisionCode' => $this->getSubdivisionCode(),
            'description' => $this->getDescription(),
            'instructions' => $this->getInstructions(),
            'incoterm' => $this->getIncoterm(),
            'currency' => null,
            'value' => null,
            'addressFrom' => $this->getAddressFrom() ? $this->getAddressFrom()->toArray() : null,
            'addressTo' => $this->getAddressTo() ? $this->getAddressTo()->toArray() : null,
            'addressInvoice' => $this->getAddressInvoice() ? $this->getAddressInvoice()->toArray() : null,
            'extra' => $this->getExtra() ? $this->getExtra()->toArray() : null,
            'parcels' => []
        ];
        foreach($this->getParcels() as $parcel) {
            $shipmentArray['parcels'][] = $parcel->toArray();
        }

        // According to Europaket the field is mandatory int, but default should be empty string, not 0
        if(!isset($shipmentArray['subdivisionCode'])) {
            $shipmentArray['subdivisionCode'] = "";
        }

        $shipmentArray = array_filter($shipmentArray, function($value) {
            // Filter null
            if(is_null($value)) {
                return false;
            }
            // Filter empty arrays
            if(is_array($value) && count($value) < 1) {
                return false;
            }
            // Rest is ok
            return true;
        });

        return $shipmentArray;
    }

}
