<?php


namespace naskorsports\europaket\Helper;

use Exception;

class Extra
{
    /** @var string|null	MaxLength: 64	Optional */
    private $reference1;
    /** @var string|null	MaxLength: 64	Optional */
    private $reference2;
    /** @var string|null	MaxLength: 64	Optional */
    private $reference3;
    /** @var string|null	MaxLength: 64	Optional */
    private $reference4;
    /** @var string|null	MaxLength: 64	Optional */
    private $reference5;

    public static function createByArray(array $extraArray): Extra
    {
        $obj = new Extra();
        $obj->setReference1($extraArray['reference1']);
        $obj->setReference2($extraArray['reference2']);
        $obj->setReference3($extraArray['reference3']);
        $obj->setReference4($extraArray['reference4']);
        $obj->setReference5($extraArray['reference5']);
        return $obj;
    }

    public function getReference1(): ?string
    {
        return $this->reference1;
    }

    /**
     * @param string|null $reference1
     * @return Extra
     * @throws Exception
     */
    public function setReference1(?string $reference1): Extra
    {
        if(strlen($reference1)>64) {
            throw new Exception("Invalid parameter");
        }
        $this->reference1 = $reference1;
        return $this;
    }

    public function getReference2(): ?string
    {
        return $this->reference2;
    }

    /**
     * @param string|null $reference2
     * @return Extra
     * @throws Exception
     */
    public function setReference2(?string $reference2): Extra
    {
        if(strlen($reference2)>64) {
            throw new Exception("Invalid parameter");
        }
        $this->reference2 = $reference2;
        return $this;
    }

    public function getReference3(): ?string
    {
        return $this->reference3;
    }

    /**
     * @param string|null $reference3
     * @return Extra
     * @throws Exception
     */
    public function setReference3(?string $reference3): Extra
    {
        if(strlen($reference3)>64) {
            throw new Exception("Invalid parameter");
        }
        $this->reference3 = $reference3;
        return $this;
    }

    public function getReference4(): ?string
    {
        return $this->reference4;
    }

    /**
     * @param string|null $reference4
     * @return Extra
     * @throws Exception
     */
    public function setReference4(?string $reference4): Extra
    {
        if(strlen($reference4)>64) {
            throw new Exception("Invalid parameter");
        }
        $this->reference4 = $reference4;
        return $this;
    }

    public function getReference5(): ?string
    {
        return $this->reference5;
    }

    /**
     * @param string|null $reference5
     * @return Extra
     * @throws Exception
     */
    public function setReference5(?string $reference5): Extra
    {
        if(strlen($reference5)>64) {
            throw new Exception("Invalid parameter");
        }
        $this->reference5 = $reference5;
        return $this;
    }

    public function toArray()
    {
        $extraArray = [
            'reference1' => $this->getReference1(),
            'reference2' => $this->getReference2(),
            'reference3' => $this->getReference3(),
            'reference4' => $this->getReference4(),
            'reference5' => $this->getReference5(),
        ];

        $extraArray = array_filter($extraArray, function($value) {
            return !is_null($value);
        });

        return $extraArray;
    }
}