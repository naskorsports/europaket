<?php

namespace naskorsports\europaket\Helper;

class TrackStatusLocation {

    /** @var string|null */
    private $location;

    /** @var string|null */
    private $city;

    /** @var string|null */
    private $country;

    /**
     * @param array $trackStatusLocationArray
     * @return TrackStatusLocation
     */
    public static function createByArray(array $trackStatusLocationArray): TrackStatusLocation
    {
        $obj = new self();
        $obj->setLocation($trackStatusLocationArray['location']??null)
            ->setCity($trackStatusLocationArray['city']??null)
            ->setCountry($trackStatusLocationArray['country']??null)
        ;
        return $obj;
    }

    /**
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string|null $location
     * @return TrackStatusLocation
     */
    private function setLocation(?string $location): TrackStatusLocation
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return TrackStatusLocation
     */
    private function setCity(?string $city): TrackStatusLocation
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return TrackStatusLocation
     */
    private function setCountry(?string $country): TrackStatusLocation
    {
        $this->country = $country;
        return $this;
    }

}
