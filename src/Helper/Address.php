<?php

namespace naskorsports\europaket\Helper;

use Exception;

class Address {

    /** @var string name	MaxLength: 64	Mandatory */
    private $name;
    /** @var string street1	MaxLength: 64	Mandatory */
    private $street1;
    /** @var string street2	MaxLength: 64	Optional */
    private $street2;
    /** @var string houseNumber	MaxLength: 16	Mandatory */
    private $houseNumber;
    /** @var string zipcode	MaxLength: 16	Mandatory */
    private $zipcode;
    /** @var string city	MaxLength: 64	Mandatory */
    private $city;
    /** @var string state	MaxLength: 16	Mandatory */
    private $state;
    /** @var string country MaxLength: 3 (ISO 3166 Alpha-2) Mandatory */
    private $country;
    /** @var string contactPerson	MaxLength: 64	Optional */
    private $contactPerson;
    /** @var string contactPhone	MaxLength: 32	Optional */
    private $contactPhone;
    /** @var string emailAddress	MaxLength: 256	Optional */
    private $emailAddress;
    /** @var string vatNumber	MaxLength: 16 	Optional */
    private $vatNumber;
    /** @var string eoriNumber  MaxLength: 32	Optional */
    private $eoriNumber;
    /** @var string accountNumber	MaxLength: 16	Optional */
    private $accountNumber;

    /**
     * @param array $addressArray
     * @return Address
     * @throws Exception
     */
    public static function createByArray(array $addressArray): Address
    {
        $obj = new Address();
        $obj->setName($addressArray['name']);
        $obj->setStreet1($addressArray['street1']);
        $obj->setStreet2($addressArray['street2']);
        $obj->setHouseNumber($addressArray['houseNumber']);
        $obj->setZipcode($addressArray['zipcode']);
        $obj->setCity($addressArray['city']);
        $obj->setState($addressArray['state']);
        $obj->setCountry($addressArray['country']);
        $obj->setContactPerson($addressArray['contactPerson']);
        $obj->setContactPhone($addressArray['contactPhone']);
        $obj->setEmailAddress($addressArray['emailAddress']);
        $obj->setVatNumber($addressArray['vatNumber']);
        return $obj;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Address
     * @throws Exception
     */
    public function setName(string $name): Address
    {
        if (strlen($name) > 64) {
            throw new Exception("Invalid name");
        }
        $this->name = $name;
        return $this;
    }

    public function getStreet1(): ?string
    {
        return $this->street1;
    }

    /**
     * @param string|null $street1
     * @return Address
     * @throws Exception
     */
    public function setStreet1(?string $street1): Address
    {
        if (strlen($street1) > 64) {
            throw new Exception("Invalid street1");
        }
        $this->street1 = $street1;
        return $this;
    }

    public function getStreet2(): ?string
    {
        return $this->street2;
    }

    /**
     * @param string|null $street2
     * @return Address
     * @throws Exception
     */
    public function setStreet2(?string $street2): Address
    {
        if (strlen($street2) > 64) {
            throw new Exception("Invalid street2");
        }
        $this->street2 = $street2;
        return $this;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    /**
     * @param string|null $houseNumber
     * @return Address
     * @throws Exception
     */
    public function setHouseNumber(?string $houseNumber): Address
    {
        if (strlen($houseNumber) > 16) {
            throw new Exception("Invalid houseNumber");
        }
        $this->houseNumber = $houseNumber;
        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    /**
     * @param string|null $zipcode
     * @return Address
     * @throws Exception
     */
    public function setZipcode(?string $zipcode): Address
    {
        if (strlen($zipcode) > 16) {
            throw new Exception("Invalid zipcode");
        }
        $this->zipcode = $zipcode;
        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return Address
     * @throws Exception
     */
    public function setCity(?string $city): Address
    {
        if (strlen($city) > 64) {
            throw new Exception("Invalid city");
        }
        $this->city = $city;
        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @param string|null $state
     * @return Address
     * @throws Exception
     */
    public function setState(?string $state): Address
    {
        if (strlen($state) > 16) {
            throw new Exception("Invalid state");
        }
        $this->state = $state;
        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return $this
     * @throws Exception
     *
     * Todo: Validate ISO-3166 Alpha-2 Code
     *       since passing an invalid value will result in HTTP Error 500 on server side.
     */
    public function setCountry(?string $country): Address
    {
        if (!preg_match('/^[A-Z]{2}$/',$country)) {
            throw new Exception("Country must be ISO 3166 Alpha 2");
        }
        $this->country = $country;
        return $this;
    }

    public function getContactPerson(): ?string
    {
        return $this->contactPerson;
    }

    /**
     * @param string|null $contactPerson
     * @return Address
     * @throws Exception
     */
    public function setContactPerson(?string $contactPerson): Address
    {
        if (strlen($contactPerson) > 64) {
            throw new Exception("Invalid contact person");
        }
        $this->contactPerson = $contactPerson;
        return $this;
    }

    public function getContactPhone(): ?string
    {
        return $this->contactPhone;
    }

    /**
     * @param string|null $contactPhone
     * @return $this
     * @throws Exception
     */
    public function setContactPhone(?string $contactPhone): Address
    {
        if (strlen($contactPhone) > 32) {
            throw new Exception("Invalid contact phone");
        }
        $this->contactPhone = $contactPhone;
        return $this;
    }

    public function getEmailAddress(): ?string
    {
        return $this->emailAddress;
    }

    /**
     * @param string|null $emailAddress
     * @return Address
     * @throws Exception
     */
    public function setEmailAddress(?string $emailAddress): Address
    {
        if (strlen($emailAddress) > 256 && filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) {
            throw new Exception("Invalid email");
        }
        $this->emailAddress = $emailAddress;
        return $this;
    }

    public function getVatNumber(): ?string
    {
        return $this->vatNumber;
    }

    /**
     * @param string|null $vatNumber
     * @return $this
     * @throws Exception
     */
    public function setVatNumber(?string $vatNumber): Address
    {
        if (strlen($vatNumber) > 16) {
            throw new Exception("Invalid vat");
        }
        $this->vatNumber = $vatNumber;
        return $this;
    }

    public function getEoriNumber(): ?string
    {
        return $this->eoriNumber;
    }

    /**
     * @param string|null $eoriNumber
     * @return Address
     * @throws Exception
     */
    public function setEoriNumber(?string $eoriNumber): Address
    {
        if (strlen($eoriNumber) > 32) {
            throw new Exception("Invalid eori number");
        }
        $this->eoriNumber = $eoriNumber;
        return $this;
    }

    public function getAccountNumber(): ?string
    {
        return $this->accountNumber;
    }

    /**
     * @param string|null $accountNumber
     * @return $this
     * @throws Exception
     */
    public function setAccountNumber(?string $accountNumber): Address
    {
        if (strlen($accountNumber) > 16) {
            throw new Exception("Invalid account number");
        }
        $this->accountNumber = $accountNumber;
        return $this;
    }

    public function toArray(): array
    {
        $addressArray = [
            'name' => $this->getName(),
            'street1' => $this->getStreet1(),
            'street2' => $this->getStreet2(),
            'houseNumber' => $this->getHouseNumber(),
            'zipcode' => $this->getZipcode(),
            'city' => $this->getCity(),
            'state' => $this->getState(),
            'country' => $this->getCountry(),
            'contactPerson' => $this->getContactPerson(),
            'contactPhone' => $this->getContactPhone(),
            'emailAddress' => $this->getEmailAddress(),
            'vatNumber' => $this->getVatNumber(),
            'eoriNumber' => $this->getEoriNumber(),
            'accountNumber' => $this->getAccountNumber(),
        ];

        $addressArray = array_filter($addressArray, function($value) {
            return !is_null($value);
        });

        return $addressArray;
    }

}
