<?php

namespace naskorsports\europaket\Helper;

use Exception;

class Parcel {

    public const  PARCELS_DISTANCE_UNIT_MM = 'Mm';
    public const  PARCELS_DISTANCE_UNIT_CM = 'Cm';
    public const  PARCELS_DISTANCE_UNIT_M = 'M';
    public const  PARCELS_DISTANCE_UNIT_IN = 'In';

    public const PARCELS_MASS_UNIT_G = 'G';
    public const PARCELS_MASS_UNIT_KG = 'Kg';
    public const PARCELS_MASS_UNIT_LB = 'Lb';

    private $packageCode;# = "PK"; // Todo: Evaluate and offer possible values
    private $description;# = "Goods"; // Todo: Evaluate and offer possible values
    private $quantity;# = 1; // Todo: What is that quantity? For 2 parcels I use 2 objects
    private $length;# = 0.3; # ": 0.3,
    private $width;# = 0.3; # ": 0.3,
    private $height;# = 0.3; # ": 0.3,
    private $distanceUnit;# = self::PARCELS_DISTANCE_UNIT_M; # ": "M",
    private $weight;# = 25; # ": 25,
    private $massUnit;# = self::PARCELS_MASS_UNIT_KG;

    // Vars yet only used in reponse
    private $trackingCode;
    private $statusHistory;
    private $label;
    private $labelType;

    /**
     * @param array $parcelArrays
     * @return Parcel[]
     * @throws \Exception
     */
    public static function createParcelsByArray(array $parcelArrays): array
    {
        $arr = [];
        foreach($parcelArrays as $parcelArray) {
            $arr[] = self::createByArray($parcelArray);
        }
        return $arr;
    }

    /**
     * @throws \Exception
     */
    private static function createByArray(array $parcelArray): Parcel
    {
        $obj = new Parcel();
        $obj->setTrackingCode($parcelArray['trackingCode']);
        $obj->setPackageCode(strval($parcelArray['packageCode']));
        $obj->setDescription(strval($parcelArray['description']));
        $obj->setQuantity($parcelArray['quantity']);
        $obj->setLength($parcelArray['length']);
        $obj->setWidth($parcelArray['width']);
        $obj->setHeight($parcelArray['height']);
        $obj->setWeight($parcelArray['weight']);
        $obj->setStatusHistory($parcelArray['statusHistory']);
        $obj->setLabel($parcelArray['label']);
        $obj->setLabelType($parcelArray['labelType']);

        return $obj;
    }

    public function getPackageCode(): ?string
    {
        return $this->packageCode;
    }

    /**
     * @param string $packageCode
     * @return Parcel
     * @throws Exception
     */
    public function setPackageCode(string $packageCode): Parcel
    {
        if (strlen($packageCode) > 16) {
            throw new Exception("Invalid package code");
        }
        $this->packageCode = $packageCode;
        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Parcel
     * @throws Exception
     */
    public function setDescription(string $description): Parcel
    {
        if (strlen($description) > 128) {
            throw new Exception("Invalid description");
        }
        $this->description = $description;
        return $this;
    }

    public function getQuantity(): ?int {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Parcel
     * @throws Exception
     */
    public function setQuantity(int $quantity): Parcel
    {
        if (!is_int($quantity)) {
            throw new Exception("Invalid quantity");
        }
        $this->quantity = $quantity;
        return $this;
    }

    public function getLength(): ?float {
        return $this->length;
    }

    /**
     * @param float $length
     * @return Parcel
     * @throws Exception
     */
    public function setLength(float $length): Parcel
    {
        if (!is_float($length)) {
            throw new Exception("Invalid length");
        }
        $this->length = $length;
        return $this;
    }

    public function getWidth(): ?float {
        return $this->width;
    }

    /**
     * @param float $width
     * @return Parcel
     * @throws Exception
     */
    public function setWidth(float $width): Parcel
    {
        if (!is_float($width)) {
            throw new Exception("Invalid width");
        }
        $this->width = $width;
        return $this;
    }

    public function getHeight(): ?float {
        return $this->height;
    }

    /**
     * @param float $height
     * @return Parcel
     * @throws Exception
     */
    public function setHeight(float $height): Parcel {
        if (!is_float($height)) {
            throw new Exception("Invalid height");
        }
        $this->height = $height;
        return $this;
    }

    public function getDistanceUnit(): ?string
    {
        return $this->distanceUnit;
    }

    /**
     * @param string $distanceUnit
     * @return Parcel
     * @throws Exception
     */
    public function setDistanceUnit(string $distanceUnit): Parcel
    {
//        if ($distanceUnit === 'M') {
//            throw new Exception("Invalid distance unit");
//        }
        $this->distanceUnit = $distanceUnit;
        return $this;
    }

    public function getWeight(): ?int {
        return $this->weight;
    }

    /**
     * @param int $weight
     * @return Parcel
     * @throws Exception
     */
    public function setWeight(int $weight): Parcel
    {
        if (!is_int($weight)) {
            throw new Exception("Invalid weight");
        }
        $this->weight = $weight;
        return $this;
    }

    public function getMassUnit(): ?string {
        return $this->massUnit;
    }

    /**
     * @param string $massUnit
     * @return Parcel
     * @throws Exception
     */
    public function setMassUnit(string $massUnit): Parcel
    {
//        if ($massUnit === 'Kg') {
//            throw new Exception("Invalid massUnit");
//        }
        $this->massUnit = $massUnit;
        return $this;
    }

    public function toArray(): array
    {
        # todo: package code is mandatory
        # todo: weight code is mandatory

        $parcelArray = [
            'packageCode' => $this->packageCode,
            'description' => $this->description,
            'quantity' => $this->quantity,
            'length' => $this->length,
            'width' => $this->width,
            'height' => $this->height,
            'distanceUnit' => $this->distanceUnit,
            'weight' => $this->weight,
            'massUnit' => $this->massUnit,
        ];

        $parcelArray = array_filter($parcelArray, function($value) {
            return !is_null($value);
        });

        return $parcelArray;
    }

    private function setTrackingCode($trackingCode): Parcel
    {
        $this->trackingCode = $trackingCode;
        return $this;
    }

    private function setStatusHistory($statusHistory): Parcel
    {
        $this->statusHistory = $statusHistory;
        return $this;
    }

    private function setLabel($label): Parcel
    {
        $this->label = $label;
        return $this;
    }

    private function setLabelType($labelType): Parcel
    {
        $this->labelType = $labelType;
        return $this;
    }

    /**
     * @return array
     */
    public function getStatusHistory(): array
    {
        return $this->statusHistory;
    }

    /**
     * Returns Base 64 coded string
     * @return string
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * Todo: Collect possible return values?
     * @return string
     */
    public function getLabelType(): ?string
    {
        return $this->labelType;
    }

    public function getTrackingCode(): ?string
    {
        return $this->trackingCode;
    }
}
