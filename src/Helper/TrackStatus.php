<?php


namespace naskorsports\europaket\Helper;

use Exception;

class TrackStatus
{

    /** @var int|null */
    private $id;
    /** @var string|null */
    private $reference;
    /** @var string|null */
    private $trackingCode;
    /** @var string|null ISO-8601 */
    private $statusDate;
    /** @var string|null */
    private $statusCode;
    /** @var string|null */
    private $statusDescription;
    /** @var string|null */
    private $carrierStatusCode;
    /** @var string|null */
    private $carrierStatusDescription;
    /** @var string|null */
    private $parcelEta;
    /** @var string|null */
    private $acceptedBy;
    /** @var TrackStatusLocation|null */
    private $location; #":

    public static function createByArray(array $trackStatusArray): TrackStatus
    {
        $obj = new TrackStatus();
        $obj->setId($trackStatusArray['id'])
            ->setTrackingCode($trackStatusArray['trackingCode']??null)
            ->setStatusDate($trackStatusArray['statusDate']??null)
            ->setStatusCode($trackStatusArray['statusCode']??null)
            ->setStatusDescription($trackStatusArray['statusDescription']??null)
            ->setCarrierStatusCode($trackStatusArray['carrierStatusCode']??null)
            ->setCarrierStatusDescription($trackStatusArray['carrierStatusDescription']??null)
            ->setParcelEta($trackStatusArray['parcelEta']??null)
            ->setAcceptedBy($trackStatusArray['acceptedBy']??null)
            ->setLocation(TrackStatusLocation::createByArray($trackStatusArray['location']))
        ;
        return $obj;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return TrackStatus
     */
    public function setId(?int $id): TrackStatus
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * @return string|null
     */
    public function getTrackingCode()
    {
        return $this->trackingCode;
    }

    /**
     * @param string|null $trackingCode
     * @return TrackStatus
     */
    private function setTrackingCode(?string $trackingCode): TrackStatus
    {
        $this->trackingCode = $trackingCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatusDate(): ?string
    {
        return $this->statusDate;
    }

    /**
     * @param string|null $statusDate
     * @return TrackStatus
     */
    private function setStatusDate(?string $statusDate): TrackStatus
    {
        $this->statusDate = $statusDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatusCode(): ?string
    {
        return $this->statusCode;
    }

    /**
     * @param string|null $statusCode
     * @return TrackStatus
     */
    private function setStatusCode(?string $statusCode): TrackStatus
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatusDescription(): ?string
    {
        return $this->statusDescription;
    }

    /**
     * @param string|null $statusDescription
     * @return TrackStatus
     */
    private function setStatusDescription(?string $statusDescription): TrackStatus
    {
        $this->statusDescription = $statusDescription;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCarrierStatusCode(): ?string
    {
        return $this->carrierStatusCode;
    }

    /**
     * @param string|null $carrierStatusCode
     * @return TrackStatus
     */
    private function setCarrierStatusCode(?string $carrierStatusCode): TrackStatus
    {
        $this->carrierStatusCode = $carrierStatusCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCarrierStatusDescription(): ?string
    {
        return $this->carrierStatusDescription;
    }

    /**
     * @param string|null $carrierStatusDescription
     * @return TrackStatus
     */
    private function setCarrierStatusDescription(?string $carrierStatusDescription): TrackStatus
    {
        $this->carrierStatusDescription = $carrierStatusDescription;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getParcelEta()
    {
        return $this->parcelEta;
    }

    /**
     * @param string|null $parcelEta
     * @return TrackStatus
     */
    private function setParcelEta(?string $parcelEta): TrackStatus
    {
        $this->parcelEta = $parcelEta;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAcceptedBy(): ?string
    {
        return $this->acceptedBy;
    }

    /**
     * @param string|null $acceptedBy
     * @return TrackStatus
     */
    private function setAcceptedBy(?string $acceptedBy): TrackStatus
    {
        $this->acceptedBy = $acceptedBy;
        return $this;
    }

    /**
     * @return TrackStatusLocation|null
     */
    public function getLocation(): ?TrackStatusLocation
    {
        return $this->location;
    }

    /**
     * @param TrackStatusLocation|null $location
     * @return TrackStatus
     */
    private function setLocation(?TrackStatusLocation $location): TrackStatus
    {
        $this->location = $location;
        return $this;
    }

}
