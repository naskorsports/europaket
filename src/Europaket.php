<?php

namespace naskorsports\europaket;

use naskorsports\europaket\Request\CreateShipment;
use naskorsports\europaket\Request\GetShipmentsByTrackingNumber;
use naskorsports\europaket\Request\TrackByDateRange;
use naskorsports\europaket\Request\TrackByParcel;
use Psr\Log\LoggerInterface;

class Europaket
{
    /** @var string */
    private $username;
    /** @var string */
    private $password;
    /** @var Request */
    private $request;
    /** @var LoggerInterface */
    private $logger;

    public static function createByAuthentication($username,$password): Europaket
    {
        $obj = new self();
        $obj->username = $username;
        $obj->password = $password;
        return $obj;
    }

    private function getRequest(): Request
    {
        if(!isset($this->request)) {
            $this->request = Request::createByEuropaket($this);
        }
        return $this->request;
    }

    public function createShipmentRequest(): CreateShipment
    {
        return CreateShipment::createByRequest($this->getRequest());
    }

    public function getUsername():string
    {
        return $this->username;
    }

    public function getPassword():string
    {
        return $this->password;
    }

    public function setLogger(LoggerInterface $logger): Europaket
    {
        $this->logger = $logger;
        return $this;
    }

    public function getLogger(): ?LoggerInterface
    {
        return $this->logger;
    }

    public function createTrackByParcelRequest(): TrackByParcel
    {
        return TrackByParcel::createByRequest($this->getRequest());
    }

    public function createTrackByDateRangeRequest(): TrackByDateRange
    {
        return TrackByDateRange::createByRequest($this->getRequest());
    }

    public function createGetShipmentsByTrackingNumberRequest()
    {
        return GetShipmentsByTrackingNumber::createByRequest($this->getRequest());
    }

    public function getTrackStatusesByTrackingNumber(string $trackingNumber): array
    {
        $createGetShipmentsRequest = $this->createGetShipmentsByTrackingNumberRequest();
        $createGetShipmentsRequest->setTrackingNumber($trackingNumber);
        $shipments = $createGetShipmentsRequest->send();
        $trackStatuses = [];

        foreach ($shipments as $shipment) {
            foreach ($shipment->getParcels() as $parcel) {
                $url = $parcel->getStatusHistory()['href'];
                $trackByParcelRequest = $this->createTrackByParcelRequest();
                $trackByParcelRequest->setStatusHistoryHref($url);
                $track = $trackByParcelRequest->send();
                $values = $track->getValue();
                if (!is_array($values)) {
                    return [];
                }
                foreach ($values as $value) {
                    $trackStatuses[] = $value;
                }
            }
        }

        return $trackStatuses;
    }

}
