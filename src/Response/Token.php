<?php

namespace naskorsports\europaket\Response;

use Psr\Http\Message\ResponseInterface;

class Token {

    /** @var string Should alway be "Bearer" */
    private $tokenType;
    /** @var string The token */
    private $accessToken;
    /** @var string String like 2021-01-27T15:35:36.9935592+00:00 */
    private $expiresAt;

    public static function createByJsonString($string): Token
    {
//        $bodyObj = json_decode((string) $response->getBody());
        $bodyObj = json_decode($string);
        $tokenObj = new self();
        $tokenObj->tokenType = $bodyObj->token_type;
        $tokenObj->accessToken = $bodyObj->access_token;
        $tokenObj->expiresAt = $bodyObj->expires_at;
        return $tokenObj;
    }

    public function isExpired(): bool
    {
        return false; // Todo
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

}