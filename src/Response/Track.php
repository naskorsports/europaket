<?php

namespace naskorsports\europaket\Response;

use naskorsports\europaket\Helper\TrackStatus;

class Track {

    /** @var string|null */
    private $href;
    /** @var TrackStatus[]|null */
    private $value;

    public static function createByJsonString($string): Track
    {
        $jsonArray = json_decode($string, true);
        $obj = new self();
        $obj->setHref($jsonArray['href']);
        $values = [];
        foreach($jsonArray['value'] as $value) {
            $values[] = TrackStatus::createByArray($value);
        }
        $obj->setValue($values);
        return $obj;
    }

    /**
     * @return string|null
     */
    public function getHref(): ?string
    {
        return $this->href;
    }

    /**
     * @param string|null $href
     * @return Track
     */
    private function setHref(?string $href): Track
    {
        $this->href = $href;
        return $this;
    }

    /**
     * @return TrackStatus[]|null
     */
    public function getValue(): ?array
    {
        return $this->value;
    }

    /**
     * @param TrackStatus[]|null $value
     * @return Track
     */
    private function setValue(?array $value): Track
    {
        $this->value = $value;
        return $this;
    }



}