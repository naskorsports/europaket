<?php

namespace naskorsports\europaket\Response;

use naskorsports\europaket\Helper\Address;
use naskorsports\europaket\Helper\Extra;
use naskorsports\europaket\Helper\Parcel;

class Shipment {

    // ?
    private $href; // NULL
    /** @var null|string ISO-8601 */
    private $createdAt; // 2021-03-09T09:52:32.09+00:00
    /** @var null|string ISO-8601 */
    private $bookedAt; // 2021-03-09T09:52:34.1403221+00:00
    /** @var null|int */
    private $id; // 16919137
    /** @var null|string */
    private $reference; // test-1615283551
    /** @var null|string */
    private $carrierName; // GLS NL
    /** @var null|string */
    private $carrierCode; // GLS
    /** @var null|string */
    private $serviceName; // CHEAPEST
    /** @var null|string */
    private $trackingCode; // 59280511645112
    /** @var null|string */
    private $description; // test
    /** @var null|string */
    private $trackingLink; // https://services.gls-netherlands.com/tracking/ttlink.asp?NVRL=59280511&VREF=TEST1615283551&REDIRTO=Verlader&CHK=27659&TAAL=NL
    /** @var null|string */
    private $status; // BOOK
    /** @var null|Address */
    private $addressFrom;
    /** @var null|Address */
    private $addressTo;
    /** @var null|Address */
    private $addressToInvoice;
    /** @var null|string */
    private $incoterm; // DAP
    /** @var null|float */
    private $weight; //25000.00
    /** @var null|string */
    private $weightUom; // Kg
    /** @var null|float */
    private $volume; // 27000.00
    /** @var null|string */
    private $volumeUom; // M
    /** @var null|float */
    private $value; // 0.0
    /** @var null|string */
    private $currency; // null
    /** @var null|int */
    private $numberOfParcels; // 1
    /** @var null|Parcel[] */
    private $parcels; // [{"trackingCode":"59280511645112","packageCode":"PK","description":"Parcel","quantity":1,"length":30.00,"width":30.00,"height":30.00,"weight":25000.00,"statusHistory":{"href":null},"label":"XlhBCl5GWCBERUZBVUxUIFRSQU5TU01BUlQgSEVBREVSIF5GUwpeTU5ZXlBNTl5MSDAsMF5KTUFeTFJOXkNJMjgKXlhaW1JFU0VUUFJJTlRFUl0KCQoJXlhBXkVHXlhaCgpbR0xTTE9HT1NNQUxMXQoJfkRHMDAxLkdSRiwwMjY4OCwwMjgsLDo6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6OjpPMEMsTjA3MCxNMDFFMCxNMEYsTDAzOCxMMDcwLEswM0MwLDpLMEYsSjAxRTBNMDNGUkY4MEowSEZDMDAzRkY4MEkwMUZGODAsSjA3QzBNMDdGUkY4MEgwMUZKRjg3RkY4MEgwMUZJRjAsSjA3ME4wN0ZSRjgwSDA3RkpGQzdGRjgwSDA3RkhGRTAsSTAxRTBOMDdGUkZJMDFGS0Y4N0ZGODBIMEpGRTAsSTAzRTBOMFRGSTA3RktGODdGRjgwMDNGSUZFMCxJMDdDME4wVEZJME1GODdGRjgwMDNGSUZFMCxJMDc4ME4wU0ZFMDAzRkxGODdGRjBIMEtGRTAsSTBGME8wU0ZFMDA3RkxGODdGRjBIMEtGRTAsSDAxRTBPMFNGRTAwRk1GOEZIRkgwMUZKRkUwLEgwMUUwTzBTRkUwMEZNRjhGSEZIMDFGSkZDMCxIMDNFME8wU0ZFMDFGTUYwRkhGSDAzRkhGQzNDMCxIMDNDME4wMUZSRkUwM0ZNRjBGSEZIMDNGSEZIMDQwLEgwNzgwTjAxRlJGRTAzRkhGQzBIMEYxRkhGSDAzRkZFLEgwRjgwTjAxRlJGQzA3RkhGSzAxRkZFMDAzRkZFLEgwRjgwTjAzRlJGQzBGSEZFMEowMUZGRTAwM0ZGRSxIMEY4MFMwM0ZNRkMwRkhGQzBKMDFGRkUwMDNGSEYsSDBGODBTMDNGTUZDMEZIRkMwSjAxRkZDMDAzRkhGODAsMDFGODBTMDdGTUY4MUZIRjgwSjAxRkZDMDAzRkhGRTAsMDFGODBTME9GODFGSEY4MEowM0ZGQzAwM0ZJRjAsMDFGODBSMDFGTkY4M0ZIRkwwM0ZGQzAwM0ZJRkMsMDFGODBSMDFGTkY4M0ZGRTAwMUZGQzNGRkMwMDNGSUZDLDAxRjgwUjAzRk5GODNGRkUwMDNGRkM3RkZDMDAxRkpGLDAxRkMwUjBQRjgzRkZFMDAzRkZDN0ZGQzBIMEtGLDAxRkUwUTAxRk9GMDNGRkUwMDNGRkM3RkY4MEgwS0Y4MCxIMEZFMFEwM0ZPRjAzRkZFMDA3RkZDN0ZGODBIMDNGSUY4MCxIMEhGUjA3Rk9GMDNGRkUwMDdGRkM3RkY4MEgwMUZJRjgwLEgwSEY4ME8wMUZQRjAzRkZDMDA3RkY4N0ZGMEowN0ZIRjgwLEgwSEZDME8wM0ZPRkUwM0ZGRTAwN0ZGODdGRjBKMDdGSEY4MCxIMDdGRjBOMDFGTEZERkhGRTAzRkZFMDA3RkY4RkhGSzAxRkhGODAsSDAzRkY4ME0wM0ZMRjlGSEZFMDNGRkUwMDdGRjhGSEZMMElGODAsSDAzRkhGTjBORjFGSEZFMDNGRkUwMDdGRjBGSEZMMDdGRjgwLEgwMUZIRkUwSzBORkMxRkhGRTAzRkhGODBGSEYwRkhGTDA3RkY4MCxIMDFGSUY4MEkwN0ZNRkMxRkhGRTAzRkhGQzBGSEYwRkhGTDBJRjgwLEkwN0ZWRjAzRkhGQzAzRk1GMEZKRjhGRTdGSEY4MCxJMDdGVUZFMDNGSEZDMDNGTUYwRkpGMEZLRixJMDFGVUY4MDNGSEZDMDNGTUYxRkpGMUZLRixKMFZGSDAzRkhGQzAxRk1GMUZKRjFGS0YsSjAzRlNGQzAwM0ZIRjgwMEZMRkUxRklGRTFGSkZFLEowMUZTRjgwMDNGSEY4MDBGTEZFM0ZJRkUxRkpGQyxLMDNGUUZDMEgwN0ZIRjgwMDdGS0ZFM0ZJRkUxRkpGOCxMMDdGT0ZFMEkwN0ZIRjgwMDNGS0ZDM0ZJRkUxRkpGMCxMMDNGT0ZDMEkwN0ZIRjgwMDFGS0ZDM0ZJRkUxRklGQzAsTTAxRk1GODBKMEpGODBIMDdGSkYwM0ZJRkUzRklGLE4wN0ZLRkMwSzBKRjgwSDAxRklGQzAzRklGRTNGSEZFLFAwMUZGMGdRMEY4MCwsOjo6Ojo6Ojo6Ojo6XlhBCltFTkRdCgkKCltOTE5ESS1GT1JNQVRdCgleWEEKCQoJCgleTEgwLDIKCQoJCgkKCQoJCgkKCQoKCQoJCgkKCQoJCgkKCV5GWC1OTCBOREkgTGFiZWwgMkReRlMKCQpbTElORVNdCQoJCgleRk80MCwxXkZSXkdCNzQ0LDQsOF5GUwoJXkZPNDAsODReRlJeR0I3NDQsNCw0XkZTCgleRk80MCwxNjleRlJeR0I3NDQsNCw0XkZTCgleRk80MCwzOTReRlJeR0I3NDQsMiw0XkZTCgleRk8zNyw0NDleRlJeR0I3NDcsMiw0XkZTCQoJXkZPMzcsNDUzXkZSXkdCMCw3MjgsNF5GUwoJXkZPNjQ1LDQ1M15GUl5HQjAsNTg4LDReRlMKCV5GTzc4MCw0NTNeRlJeR0IwLDcyOCw0XkZTCgleRk80MSw2ODJeRlJeR0I2MDQsMCw0XkZTCgleRk80MSw4ODReRlJeR0I2MDQsMCw0XkZTCgleRk80MSwxMDQxXkZSXkdCNzQwLDAsNF5GUwoJXkZPMzcsMTE4MF5GUl5HQjc0NywwLDReRlMKCV5GTzQwLDE3M15GUl5HQjIwLDUsNV5GUwoJXkZPNDAsMTc4XkZSXkdCNSwxNSw1XkZTCgleRk80MCwzODleRlJeR0IyMCw1LDVeRlMKCV5GTzQwLDM3NF5GUl5HQjUsMTUsNV5GUwoJXkZPMjUzLDE3M15GUl5HQjIwLDUsNV5GUwoJXkZPMjY4LDE3OF5GUl5HQjUsMTUsNV5GUwoJXkZPMjUzLDM4OV5GUl5HQjIwLDUsNV5GUwoJXkZPMjY4LDM3NF5GUl5HQjUsMTUsNV5GUwoKW0JBUkNPREVTICYgREFUQU1BVFJJWF0JCgkKCV5GTzgxLDIxNF5CWE4sNCwyMDBeRlJeRkRBTkw2NTAwTkw2NTAwNTI4MDAzOTE2NjUyODAwMDAwMDAwMUFKSkVLREFBICAgICAgICAgIDcgICAwMDQ3NTkzMiBOQjAyNTAwMDAxMDAxNTkyODA1MTE2NDUxMTIgICAgICAgIDU5MjgwNTExNjQ1MTEyICAgICAgICBeRlMKCV5GTzYwNCwyMTReQlhOLDQsMjAwXkZSXkZEQXxOYXNrb3JTcG9ydHMgQlZ8VHJhcHBpc3RlbndlZyA4fFRlZ2VsZW58fFRFU1QxNjE1MjgzNTUxfDU5MzJOQk5BU0tPfCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBeRlMKCV5GTzE4NSw5MDBeQlkzLDIuMCwyNDBeQjJOLDEwMCxOLE4sTl5GUl5GRDU5MjgwNTExNjQ1MTEyXkZTCgleRk8yNTAsMTAxMF5BME4sMjYsMjZeRlJeRkQ1OTI4MDUxMTY0NTExMl5GUwoJCltST1VUSU5HIFBBUlQgMV0JCQoJCgleRk8yNzAsOV5GUl5HQjcwLDc1LDM5XkZTCgleRk8yODAsMTReQTBOLDgzLDk4XkZSXkZEN15GUwoJXkZPNDY1LDE0XkEwTiw4Myw5OF5GUl5GRE5MXkZTCgleRk81OTUsOV5GUl5HQjE4OSw3NSwzOV5GUwoJXkZPNjAwLDE5XkEwTiw3Myw4NV5GUl5GRDY1MDBeRlMKCV5GTzUwLDk0XkEwTiw4Myw2N15GUl5GRDAwNDdeRlMKCV5GTzIyMCwxMTReQTBOLDY2LDMzXkZSXkZENTkzMiBOQl5GUwoJXkZPMjIwLDk0XkEwTiwyMCwyMF5GUl5eRkRaaXBDb2RlXkZTCgleRk8zNzAsOTReQTBOLDIwLDIwXkZSXkZEWW91ciBHTFMgVHJhY2sgSUReRlMKCV5GTzM3MCwxMzleQTBOLDMwLDMwXkZSXkZEMDFBSkpFS0ReRlMKCQpbUk9VVElORyBQQVJUIDJdCQoJCgleRk80MCw0MDVeQTBOLDQwLDMwXkZSXkZETkw2NTAwXkZTCgleRk8xMzAsNDEwXkEwTiwzMCwzMF5GUl5GRGV6XkZTCgleRk8xNzAsNDE0XkEwTiwyMCwyMF5GUl5GRDA5LjAzLjIwMjFeRlMKCV5GTzMwMCw0MTReQTBOLDIwLDIwXkZSXkZEMTA6NTJeRlMKCV5GTzM2MCw0MDVeQTBOLDQwLDMwXkZSXkZEMjUuMDBeRlMKCV5GTzQzMCw0MDVeQTBOLDQwLDMwXkZSXkZEa2deRlMKCV5GTzQ3MCw0MTReQTBOLDIwLDIwXkZSXkZEMV5GUwoJXkZPNTAwLDQxNF5BME4sMjAsMjBeRlJeRkQvXkZTCgleRk81MTAsNDE0XkEwTiwyMCwyMF5GUl5GRDFeRlMKCV5GTzU4MCw0MTReQTBOLDIwLDIwXkZSXkZEUlRHXkZTCgleRk82MjAsNDE0XkEwTiwyMCwyMF5GUl5GRDA4MDMyMDIxXkZTCgleRk83MjUsNDE0XkEwTiwxOCwxNl5GUl5GREIyLjAwLjBeRlMKCQpbUFJPRFVDVCAmIFNFUlZJQ0UgSU5GT1JNQVRJT05dCgkKCgkKW1NFTkRFUiBJTkZPUk1BVElPTl0JCgkKCV5BMFIsMjMsMjNeRk83NTAsNDY0XkZWU2VuZGVyOl5GUwoJXkEwUiwyMCwyMF5GTzc1MCw4MTBeRlZDdXN0b21lciBJRDpeRlMKCV5BMFIsMjAsMjBeRk83NTAsOTMwXkZWNTI4MDAzOTE2Nl5GUwoJXkEwUiwyMywyM15GTzcyNSw0NjReRlZOYXNrb3JTcG9ydHMgQlZeRlMKCV5BMFIsMjMsMjNeRk82NzUsNDY0XkZWVHJhcHBpc3RlbndlZyA4XkZTCgleQTBSLDIzLDIzXkZPNjUwLDQ2NF5GVk5MXkZTCgleQTBSLDIzLDIzXkZPNjUwLDUxMF5GVjU5MzIgTkJeRlMKCV5BMFIsMjMsMjNeRk82NTAsNjEwXkZWVGVnZWxlbl5GUwoJCltSRUNFSVZFUiBJTkZPUk1BVElPTl0KCQoJXkZPNTAsNzEzXkEwTiwzNywzMl5GUl5GRE5hc2tvclNwb3J0cyBCVl5GUwoJXkZPNTAsODA5XkEwTiw0MCwzM15GUl5GRFRyYXBwaXN0ZW53ZWcgOF5GUwoJXkZPNTAsODU0XkEwTiwyNiwzM15GUl5GRE5MLV5GUwoJXkZPMTE1LDg1NF5BME4sMjYsMzNeRlJeRkQ1OTMyIE5CXkZTCgleRk8yNTUsODU0XkEwTiwyNiwzM15GUl5GRFRlZ2VsZW5eRlMKCQpbQURESVRJT05BTCBJTkZPUk1BVElPTl0JCgkKCV5GTzUwLDEwNTBeQTBOLDI2LDIwXkZSXkZEQ29udGFjdF5GUwoJXkZPNTAsMTA3NV5BME4sMjYsMjBeRlJeRkROb3RlXkZTIAoJXkZPNTAsMTEwMF5BME4sMjYsMjBeRlJeRkROb3RlXkZTCgleRk81MCwxMTI1XkEwTiwyNiwyMF5GUl5GRFJlZmVyZW5jZTpeRlMKCQoJXkZPMTgwLDEwNzVeQTBOLDI2LDIwXkZSXkZEUGFyY2VsXkZTCgleRk8xODAsMTEwMF5BME4sMjYsMjBeRlJeRkR0ZXN0XkZTCgleRk8xODAsMTEyNV5BME4sMjYsMjBeRlJeRkRURVNUMTYxNTI4MzU1MV5GUwoJCgleRk81NjAsMTA3NV5YRzAwMS5HUkYsMSwxXkZTCgleUFExCgleWFoJCltFTkRdCgoK","labelType":"ZPL","deliveryNoteInformation":null}]
    /** @var null|Extra */
    private $extra; // {"reference1":"test-1615283551","reference2":null,"reference3":null,"reference4":"test-1615283551","reference5":null,"reference6":null,"reference7":null}
    // ?
    private $carrier; // {"href":null}
    // ?
    private $service; // {"href":null}
    // ?
    private $customsDeclaration; // null
    // ?
    private $deliveryNoteInformation; // null
    /** @var bool */
    private $tracked; // true
    /** @var null|int */
    private $notificationId; // 0

    /**
     * @throws \Exception
     */
    public static function createByJsonString($string): Shipment
    {
        $jsonArray = json_decode($string,true);
        $obj = new Shipment();
        if (isset($jsonArray['createdAt'])) {
            $obj->setCreatedAt($jsonArray['createdAt']);
        }
        if(isset($jsonArray['bookedAt'])) {
            $obj->setBookedAt($jsonArray['bookedAt']);
        }
        if(isset($jsonArray['id'])) {
            $obj->setId($jsonArray['id']);
        }
        if(isset($jsonArray['reference'])) {
            $obj->setReference($jsonArray['reference']);
        }
        if(isset($jsonArray['carrierName'])) {
            $obj->setCarrierName($jsonArray['carrierName']);
        }
        if(isset($jsonArray['carrierCode'])) {
            $obj->setCarrierCode($jsonArray['carrierCode']);
        }
        if(isset($jsonArray['serviceName'])) {
            $obj->setServiceName($jsonArray['serviceName']);
        }
        if(isset($jsonArray['trackingCode'])) {
            $obj->setTrackingCode($jsonArray['trackingCode']);
        }
        if(isset($jsonArray['description'])) {
            $obj->setDescription($jsonArray['description']);
        }
        if(isset($jsonArray['trackingLink'])) {
            $obj->setTrackingLink($jsonArray['trackingLink']);
        }
        if(isset($jsonArray['status'])) {
            $obj->setStatus($jsonArray['status']);
        }
        if(isset($jsonArray['addressFrom'])) {
            $obj->setAddressFrom(Address::createByArray($jsonArray['addressFrom']));
        }
        if(isset($jsonArray['addressTo'])) {
            $obj->setAddressTo(Address::createByArray($jsonArray['addressTo']));
        }
        if(isset($jsonArray['addressToInvoice'])) {
            $obj->setAddressToInvoice(Address::createByArray($jsonArray['addressToInvoice']));
        }
        if(isset($jsonArray['incoterm'])) {
            $obj->setIncoterm($jsonArray['incoterm']);
        }
        if(isset($jsonArray['weight'])) {
            $obj->setWeight($jsonArray['weight']);
        }
        if(isset($jsonArray['weightUom'])) {
            $obj->setWeightUom($jsonArray['weightUom']);
        }
        if(isset($jsonArray['volume'])) {
            $obj->setVolume($jsonArray['volume']);
        }
        if(isset($jsonArray['volumeUom'])) {
            $obj->setVolumeUom($jsonArray['volumeUom']);
        }
        if(isset($jsonArray['value'])) {
            $obj->setValue($jsonArray['value']);
        }
        if(isset($jsonArray['currency'])) {
            $obj->setCurrency($jsonArray['currency']);
        }
        if(isset($jsonArray['numberOfParcels'])) {
            $obj->setNumberOfParcels($jsonArray['numberOfParcels']);
        }
        if(isset($jsonArray['parcels'])) {
            $obj->setParcels(Parcel::createParcelsByArray($jsonArray['parcels']));
        }
        if(isset($jsonArray['extra'])) {
            $obj->setExtra(Extra::createByArray($jsonArray['extra']));
        }
        $obj->setTracked(isset($jsonArray['tracked']) ? true : false);
        if(isset($jsonArray['notificationId'])) {
            $obj->setNotificationId($jsonArray['notificationId']);
        }

        return $obj;
    }

    private function setCreatedAt(string $createdAt): Shipment
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    private function setBookedAt(string $bookedAt): Shipment
    {
        $this->bookedAt = $bookedAt;
        return $this;
    }

    private function setId(int $id): Shipment
    {
        $this->id = $id;
        return $this;
    }

    private function setReference(?string $reference): Shipment
    {
        $this->reference = $reference;
        return $this;
    }

    private function setCarrierName(?string $carrierName): Shipment
    {
        $this->carrierName = $carrierName;
        return $this;
    }

    private function setCarrierCode(?string $carrierCode): Shipment
    {
        $this->carrierCode = $carrierCode;
        return $this;
    }

    private function setServiceName(?string $serviceName): Shipment
    {
        $this->serviceName = $serviceName;
        return $this;
    }

    private function setTrackingCode(?string $trackingCode): Shipment
    {
        $this->trackingCode = $trackingCode;
        return $this;
    }

    private function setDescription(?string $description): Shipment
    {
        $this->description = $description;
        return $this;
    }

    private function setTrackingLink(?string $trackingLink): Shipment
    {
        $this->trackingLink = $trackingLink;
        return $this;
    }

    private function setStatus(?string $status): Shipment
    {
        $this->status = $status;
        return $this;
    }

    private function setAddressFrom(Address $addressFrom): Shipment
    {
        $this->addressFrom = $addressFrom;
        return $this;
    }

    private function setAddressTo(Address $addressTo): Shipment
    {
        $this->addressTo = $addressTo;
        return $this;
    }

    private function setAddressToInvoice(Address $addressToInvoice): Shipment
    {
        $this->addressToInvoice = $addressToInvoice;
        return $this;
    }

    private function setIncoterm(?string $incoterm): Shipment
    {
        $this->incoterm = $incoterm;
        return $this;
    }

    private function setWeight(?float $weight): Shipment
    {
        $this->weight = $weight;
        return $this;
    }

    private function setWeightUom(?string $weightUom): Shipment
    {
        $this->weightUom = $weightUom;
        return $this;
    }

    private function setVolume(?float $volume): Shipment
    {
        $this->volume = $volume;
        return $this;
    }

    private function setVolumeUom(?string $volumeUom): Shipment
    {
        $this->volumeUom = $volumeUom;
        return $this;
    }

    private function setValue(?float $value): Shipment
    {
        $this->value = $value;
        return $this;
    }

    private function setCurrency(?string $currency): Shipment
    {
        $this->currency = $currency;
        return $this;
    }

    private function setNumberOfParcels(?int $numberOfParcels): Shipment
    {
        $this->numberOfParcels = $numberOfParcels;
        return $this;
    }

    /**
     * @param Parcel[] $parcels
     * @return Shipment
     */
    private function setParcels(array $parcels): Shipment
    {
        $this->parcels = $parcels;
        return $this;
    }

    private function setExtra(Extra $extra): Shipment
    {
        $this->extra = $extra;
        return $this;
    }

    private function setTracked(bool $tracked): Shipment
    {
        $this->tracked = $tracked;
        return $this;
    }

    private function setNotificationId(?int $notificationId): Shipment
    {
        $this->notificationId = $notificationId;
        return $this;
    }

    /**
     * @return Parcel[]
     */
    public function getParcels(): array
    {
        return $this->parcels;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getBookedAt(): ?string
    {
        return $this->bookedAt;
    }

    /**
     * @return string
     */
    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * @return string
     */
    public function getCarrierName(): ?string
    {
        return $this->carrierName;
    }

    /**
     * @return string
     */
    public function getCarrierCode(): ?string
    {
        return $this->carrierCode;
    }

    /**
     * @return string
     */
    public function getServiceName(): ?string
    {
        return $this->serviceName;
    }

    /**
     * @return string
     */
    public function getTrackingCode(): ?string
    {
        return $this->trackingCode;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getTrackingLink(): ?string
    {
        return $this->trackingLink;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @return Address
     */
    public function getAddressFrom(): ?Address
    {
        return $this->addressFrom;
    }

    /**
     * @return Address
     */
    public function getAddressTo(): ?Address
    {
        return $this->addressTo;
    }

    /**
     * @return Address
     */
    public function getAddressToInvoice(): ?Address
    {
        return $this->addressToInvoice;
    }

    /**
     * @return string
     */
    public function getIncoterm(): ?string
    {
        return $this->incoterm;
    }

    /**
     * @return float
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @return string
     */
    public function getWeightUom(): ?string
    {
        return $this->weightUom;
    }

    /**
     * @return float
     */
    public function getVolume(): ?float
    {
        return $this->volume;
    }

    /**
     * @return string
     */
    public function getVolumeUom(): ?string
    {
        return $this->volumeUom;
    }

    /**
     * @return float
     */
    public function getValue(): ?float
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @return int
     */
    public function getNumberOfParcels(): ?int
    {
        return $this->numberOfParcels;
    }

    /**
     * @return Extra
     */
    public function getExtra(): ?Extra
    {
        return $this->extra;
    }

    /**
     * @return bool
     */
    public function isTracked(): bool
    {
        return $this->tracked;
    }

    /**
     * @return int
     */
    public function getNotificationId(): ?int
    {
        return $this->notificationId;
    }

}
